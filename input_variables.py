# Script for changing inlet conditions for 3D halfbody simulations
# Urszula Golyska 2021 c
import math
from distutils.dir_util import copy_tree
import numpy as np
import os

def change_in_file(dir_name, string_to_compare, value):
    # Change file contents
    old_file = open(dir_name, 'r')
    new_file = open(dir_name+"_new", 'w')

    for line in old_file:
        if string_to_compare in line:   # find line in old file which contains the name of the variable to be changed
            s = string_to_compare + "       " + str(value) +";\n"   # write new variable value
            new_file.writelines(s)  # save in now file
        else:
            new_file.writelines(line)   # otherwise write old line to new file

    old_file.close()
    new_file.close()

    os.remove(dir_name)
    os.rename(dir_name+'_new', dir_name)

def create_decompose(dir_name, proc_case):
    # how to split the domain
    # default - all splits in streamwise direction
    ny = 1
    nz = proc_case

    if int(math.sqrt(proc_case)+0.5)**2 == proc_case: # if the number of used processors is a perfect square - divide equally in both directions
        ny = int(math.sqrt(proc_case))
        nz = int(math.sqrt(proc_case))
    elif  proc_case%2 == 0: # if number of used processors is divisible by two - use two in z directions, rest in streamwise direction
        ny = 2
        nz = int(proc_case/ny)

    # Change input file
    decomp_file = open(dir_name + "/system/decomposeParDict", 'w')

    header = "/*--------------------------------*- C++ -*----------------------------------*\n  =========                 |\n  \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox\n   \\\\    /   O peration     | Website:  https://openfoam.org\n    \\\\  /    A nd           | Version:  7\n     \\\\/     M anipulation  |\n\*---------------------------------------------------------------------------*/\n" \
             "FoamFile\n{\nversion     2.0;\nformat      ascii;\nclass       dictionary;\nobject      decomposeParDict;\n}\n\n// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n\n"
    decomp_file.writelines(header)
    # save number of used processors into file
    decomp_file.writelines("numberOfSubdomains "+ str(proc_case)+ ";\n\n")
    # save division along specific axis into file
    decomp_file.writelines("method		simple;\n\nsimpleCoeffs\n{\nn               (1 " + str(ny) + " " + str(nz) + "); \ndelta           0.001;\n}\n\n// ************************************************************************* //")
    decomp_file.close()

def file_create_func(U, alpha, turb, mac, dir_name):
    # copy subdirectory
    copy_tree('basic', dir_name)

    # velocity components
    Uz = -U * math.cos(math.radians(alpha))
    Uy = U * math.sin(math.radians(alpha))

    # Change input file
    input_file = open(dir_name + "/0/include/initialConditions", 'w')

    header = "/*--------------------------------*- C++ -*----------------------------------*\n  =========                 |\n  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox\n   \\    /   O peration     | Website:  https://openfoam.org\n    \\  /    A nd           | Version:  7\n     \\/     M anipulation  |\n\*---------------------------------------------------------------------------*/\n\n"

    input_file.writelines(header)

    # change inlet conditions
    s = "flowVelocity   (0 " + str(Uy) + " " + str(Uz) + ");\n" # velocity
    input_file.writelines(s)

    # turbulent kinetic energy at inlet
    k = 3/2*(turb*U)**2;
    s = "turbulentK   " + str(k)+";\n"  # k
    input_file.writelines(s)

    # specific turbulent dissipation rate
    omega = k**0.5/(0.09**0.25*mac); # C_mu = 0.09 is a constant
    s = "turbulentOmega   " + str(omega)+ ";\n\n"  # omega
    input_file.writelines(s)

    foot = "// ************************************************************************* //\n"
    input_file.writelines(foot)
    input_file.close()


if __name__ == '__main__':

    # user input, specifying cases to run
    # velocity range
    U_array = input("Enter freestream velocity (in m/s): ")
    if ":" in U_array:  # for a range, use : -> U_start:U_step:U_end
        temp = U_array.split(":")
        U_array = np.arange(float(temp[0]), float(temp[2])+float(temp[1]), float(temp[1]))
    elif " " in U_array:    # for singular values, use space as delimiter
        U_array = U_array.split()
        for i in range(len(U_array)):
            U_array[i] = float(U_array[i])
    else:
        U_array = [float(U_array)]  # for only one velocity value, has to be a list

    # angle of attack range
    alpha_array = input("Enter angle of attack (in degrees): ")
    if ":" in alpha_array:
        temp = alpha_array.split(":")   # for a range, use : -> aoa_start:aoa_step:aoa_end
        alpha_array = np.arange(float(temp[0]), float(temp[2])+float(temp[1]), float(temp[1]))
    elif " " in alpha_array:    # for singular values, use space as delimiter
        alpha_array = alpha_array.split()
        for i in range(len(alpha_array)):
            alpha_array[i] = float(alpha_array[i])
    else:
        alpha_array = [float(alpha_array)]  # for only one angle of attack, has to be a list

    # turbulence intensity of the flow
    turb = input("Enter turbulence intensity, default is 0.01 (max. recommended value for external flows): ")
    if turb == "":
        turb = 0.01 # default value if no other is specified
    else:
        turb = float(turb)

    # mean aerodynamic chord in meters, used only for inlet condition estimation, so doesn't have to be exact
    mac = float(input("Enter MAC [m]: "))

    # maximum number of iterations per case
    iter = input("Enter maximum number of iterations (default is 500): ")
    if iter == "":
        iter = 500 # default value if no other is specified
    else:
        iter = float(iter)

    # if number of iterations is different than default, change in controlDict file
    if iter!=500:
        change_in_file("basic/system/controlDict", "endTime", iter)

    # minimum residual for pressure and velocity
    residual = input("Enter minimum residual (default is 5e-5): ")
    if residual == "":
        residual = 5e-5 # default value if no other is specified
    else:
        residual = float(residual)

    # if residual is different than default, change in fvSolution file
    if residual!=5e-5:
        change_in_file("basic/system/fvSolution", "(p|U)", residual)

    # for all velocities and angles of attack - copy basic folder and make necessary changes
    for U in U_array:
        for alpha in alpha_array:
            dir_name = "U_" + str(U) + "_aoa_" + str(alpha)  # default directory in current folder
            file_create_func(U, alpha, turb, mac, dir_name)

    # for now only one by one on one processor each
    # create all run file
    # run_file = open("Allrun_cases2", 'w')
    # run_file.writelines('# !/bin/bash\n\n')

    ## for single processor use, running cases one by one
    # for U in U_array:
    #     for alpha in alpha_array:
    #         dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
    #         run_file.writelines("simpleFoam -case " + str(dir_name) + "> log" + str(dir_name) + ".log \n")


    nr_proc = input("Number of processors dedicated for running simulations (1 is default): ")
    proc_case = input("Number of processors per case (1 is default): ")

    if nr_proc == "":
        nr_proc = 1
    else:
        nr_proc = int(nr_proc)

    if proc_case == "":
        proc_case = 1
    else:
        proc_case = int(proc_case)

    nr_par = int(nr_proc/proc_case) # number of parallel processes

    # create all run file
    run_file = open("Allrun_cases", 'wb')
    run_file.write(b'# !/bin/bash\n\n')

    if proc_case > 1:
        for U in U_array:
            for alpha in alpha_array:
                dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
                create_decompose(dir_name,proc_case) # create decomposeParDict
                run_file.write(b'decomposePar -case ' + dir_name.encode('ascii') + b'\n')

    count = 0;
    for U in U_array:
        for alpha in alpha_array:
            count = count+1;
            dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
            if proc_case>1:
                if count % nr_par == 0: # last parallel process in batch
                    run_file.write(b'mpirun -np ' + str(proc_case).encode('ascii') + b' simpleFoam -case ' + dir_name.encode('ascii') + b' -parallel > log' + dir_name.encode('ascii') + b'.log \n')
                else:
                    run_file.write(b'mpirun -np ' + str(proc_case).encode('ascii') + b' simpleFoam -case ' + dir_name.encode('ascii') + b' -parallel > log' + dir_name.encode('ascii') + b'.log &\n')

            else:   # serial computing with one processor
                run_file.write(b'simpleFoam -case ' + dir_name.encode('ascii') + b' > log' + dir_name.encode('ascii') + b'.log \n')

    if proc_case > 1:
        for U in U_array:
            for alpha in alpha_array:
                dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
                run_file.write(b'reconstructPar -case ' + dir_name.encode('ascii') + b' -latestTime\n')

    run_file.close()