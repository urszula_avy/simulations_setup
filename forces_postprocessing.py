# Script for poostprocessing aerodynamic coefficients for 3D halfbody simulations
# Urszula Golyska 2021 c
import glob, os
import numpy as np

def loop_func(fx,fy,fz,mx,my,mz,dir_name):
    # open file
    post_file = open(dir_name+"/postProcessing/forces1/0/forces.dat", 'r')
    for line in reversed(open(dir_name+"/postProcessing/forces1/0/forces.dat").readlines()):    # loop from the end of the file
           if line!="": # last entry
               coeffs = line.split()

               # add pressure and viscosity forces
               fx_temp = float(coeffs[1].replace("(", "")) + float(coeffs[4].replace("(", ""))
               fy_temp = float(coeffs[2]) + float(coeffs[5])
               fz_temp = float(coeffs[3].replace(")", "")) + float(coeffs[6].replace(")", ""))

               # add pressure and viscosity momentums
               mx_temp = float(coeffs[10].replace("(", "")) + float(coeffs[13].replace("(", ""))
               my_temp = float(coeffs[11]) + float(coeffs[14])
               mz_temp = float(coeffs[12].replace(")", "")) + float(coeffs[15].replace(")", ""))

                # append to list
               fx.append(fx_temp)
               fy.append(fy_temp)
               fz.append(fz_temp)
               mx.append(mx_temp)
               my.append(my_temp)
               mz.append(mz_temp)
               break

if __name__ == '__main__':

       # user input, specify cases to postprocess
       # velocity range
       U_array = input("Enter freestream velocity (in m/s): ")
       if ":" in U_array:
              temp = U_array.split(":") # for a range, use : -> U_start:U_step:U_end
              U_array = np.arange(float(temp[0]), float(temp[2]) + float(temp[1]), float(temp[1]))
       elif " " in U_array: # for singular values, use space as delimiter
              U_array = U_array.split()
              for i in range(len(U_array)):
                     U_array[i] = float(U_array[i])
       else:
              U_array = [float(U_array)] # for only one velocity value, has to be a list

       # angle of attack range
       alpha_array = input("Enter angle of attack (in degrees): ")
       if ":" in alpha_array:
              temp = alpha_array.split(":") # for a range, use : -> aoa_start:aoa_step:aoa_end
              alpha_array = np.arange(float(temp[0]), float(temp[2]) + float(temp[1]), float(temp[1]))
       elif " " in alpha_array: # for singular values, use space as delimiter
              alpha_array = alpha_array.split()
              for i in range(len(alpha_array)):
                     alpha_array[i] = float(alpha_array[i])
       else:
              alpha_array = [float(alpha_array)]    # for only one angle of attack, has to be a list

       fx = [];
       fy = [];
       fz = [];
       mx = [];
       my = [];
       mz = [];

       # save forces to txt file
       f = open("forces_output.txt", "w+")
       f.write("U[m/s] alpha[deg] Fx[N] Fy[N] Fz[N] Mx[Nm] My[Nm] Mz[Nm]\n")    # header

    # loop over all specified velocities and angles of attack
       for U in U_array:
              for alpha in alpha_array:
                     dir_name = "U_" + str(U) + "_aoa_" + str(alpha)  # default directory in current folder

                     # calculate final forces and momentum
                     loop_func(fx,fy,fz,mx,my,mz,dir_name)

                     # write to file
                     f.write(str(U)+" "+str(alpha)+" "+str(fx[-1])+" "+str(fy[-1])+" "+str(fz[-1])+" "+str(mx[-1])+" "+str(my[-1])+" "+str(mz[-1])+"\n")
       f.close()